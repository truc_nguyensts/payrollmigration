﻿using EmpInfo_Migration.DbContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NetCore.Shared.Extentions;
using NetCore.Tools.Migration.DbContexts.Payroll;
using System;

namespace NetCore.Tools.Migration
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    var application = scope.ServiceProvider.GetRequiredService<MigrationService>();
                    application.Run(args);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return;
                }
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseEnvironment(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"))
                .ConfigureAppConfiguration((HostBuilderContext hostBuilderContext, IConfigurationBuilder configurationBuilder) => configurationBuilder.AddAppSettings(hostBuilderContext, args))
                .ConfigureServices((hostBuilderContext, services) =>
                {
                    var empInforConnection = hostBuilderContext.Configuration.GetValue<string>("ConnectionStrings:EmpInforConnection");
                    var payrollConnection = hostBuilderContext.Configuration.GetValue<string>("ConnectionStrings:PayrollContext");
                    AppSettingsModel.SalaryEncryptKey = hostBuilderContext.Configuration.GetValue<string>("ConnectionStrings:SalaryEncryptKey");
                    AppSettingsModel.PayrollSalaryEncryptKey = hostBuilderContext.Configuration.GetValue<string>("ConnectionStrings:PayrollSalaryEncryptKey");

                    services.AddDbContext<EmpInfoDbContext>(builder =>
                    {
                        builder.UseSqlServer(empInforConnection);
                    });

                    services.AddDbContext<PayrollDbContext>(builder =>
                    {
                        builder.UseSqlServer(payrollConnection);
                    });

                    services.AddScoped<MigrationService>();
                })
                .AddLoggingConfiguration("netcore-migration-logs"); ;
        }
    }
}
