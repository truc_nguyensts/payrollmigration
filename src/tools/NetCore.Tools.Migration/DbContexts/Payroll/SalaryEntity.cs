﻿using System;

namespace NetCore.Tools.Migration.DbContexts.Payroll
{
    public class SalaryEntity
    {
        public int Id { get; set; }
        public string EncryptedGrossSalary { get; set; }
        public string EncryptedNetSalary { get; set; }
        public string EncryptedBaseSalary { get; set; }
        public bool CcFullCover { get; set; }
        public DateTimeOffset ApplyDate { get; set; }
        public DateTimeOffset? LastApplyDate { get; set; }
        public int EmployeeId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedTime { get; set; }
    }
}
