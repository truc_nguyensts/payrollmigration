﻿using System;

namespace NetCore.Tools.Migration.DbContexts.EmpInfo.EmpInfoEntities
{
    public class SocialEnsuranceEntity
    {
        public int Id { get; set; }
        public double Percentage { get; set; }
        public DateTime ApplyStartDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
