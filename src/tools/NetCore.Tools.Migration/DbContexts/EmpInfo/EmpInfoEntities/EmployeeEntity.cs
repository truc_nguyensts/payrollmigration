﻿namespace EmpInfo_Migration.DbContexts.EmpInfo.EmpInfoEntities
{
    public class EmployeeEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public virtual UserEntity User { get; set; }
    }
}
