﻿using EmpInfo_Migration.DbContexts.EmpInfo.EmpInfoEntities;
using Microsoft.EntityFrameworkCore;
using NetCore.Tools.Migration.DbContexts.EmpInfo.EmpInfoEntities;

namespace EmpInfo_Migration.DbContexts
{
    public class EmpInfoDbContext : DbContext
    {
        public EmpInfoDbContext(DbContextOptions<EmpInfoDbContext> options) : base(options)
        {

        }

        public DbSet<ContractEntity> Contracts { get; set; }
        public DbSet<SocialEnsuranceEntity> SocialEnsurances { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContractEntity>(entity =>
            {
                entity.ToTable("Contract");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.ChangeContractDate).HasColumnName("ChangeContractDate");
                entity.Property(e => e.IsSalaryChange).HasColumnName("IsSalaryChange");
                entity.Property(e => e.NetSalary).HasColumnName("NetSalary");
                entity.Property(e => e.GrossSalary).HasColumnName("GrossSalary");
                entity.Property(e => e.BaseSalary).HasColumnName("BaseSalary");
                entity.Property(e => e.SocialEnsurancePercentage).HasColumnName("SocialEnsurancePercentage");
                entity.Property(e => e.EmployeeId).HasColumnName("EmployeeId");
                entity.Property(e => e.IsDeleted).HasColumnName("IsDeleted");
            });

            modelBuilder.Entity<EmployeeEntity>(entity =>
            {
                entity.ToTable("Employee");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.UserId).HasColumnName("UserId");
            });

            modelBuilder.Entity<UserEntity>(entity =>
            {
                entity.ToTable("User");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.EmpCode).HasColumnName("EmpCode");
            });

            modelBuilder.Entity<SocialEnsuranceEntity>(entity =>
            {
                entity.ToTable("SocialEnsurance");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Percentage).HasColumnName("Percentage");
                entity.Property(e => e.ApplyStartDate).HasColumnName("ApplyStartDate");
                entity.Property(e => e.IsDeleted).HasColumnName("IsDeleted");
            });
        }
    }
}
