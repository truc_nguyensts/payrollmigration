﻿using System;

namespace NetCore.Tools.Migration.Models
{
    public class SalaryModel
    {
        public int EmployeeId { get; set; }
        public DateTime ApplyDate { get; set; }
        public string EncryptedNetSalary { get; set; }
        public string EncryptedGrossSalary { get; set; }
        public string EncryptedBaseSalary { get; set; }
        public double? SocialEnsurancePercentage { get; set; }
    }
}
