﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCore.Tools.Migration.Models
{
    public class ContractModel
    {
        public string EmpCode { get; set; }
        public DateTime ChangeContractDate { get; set; }
        public decimal NetSalary { get; set; }
        public decimal GrossSalary { get; set; }
        public decimal BaseSalary { get; set; }
        public double? SocialEnsurancePercentage { get; set; }
    }
}
