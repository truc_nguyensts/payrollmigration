﻿using STSInternal.Common.Crypto;
using STSInternal.Common.Text;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCore.Tools.Migration.Helpers
{
    public static class EncryptionHelper
    {
        const char SalaryEncryptionDelimeter = ':';
        public static IReadOnlyList<string> EncryptEmployeeSalaries(IEnumerable<decimal> salaries, string cryptoKey)
        {
            using var cryptoAes = DataPrivacy.CreateAes();
            var utf8Encoding = Encoding.UTF8;
            var keyBytes = utf8Encoding.GetBytes(cryptoKey);
            cryptoAes.SetKey(keyBytes);

            var encryptedSalaries = salaries.Select(salary =>
            {
                var encryptedBytes = cryptoAes.Encrypt(salary.ToString());
                var encryptedText = TextHelper.Base64Encode(encryptedBytes);
                var ivText = TextHelper.Base64Encode(cryptoAes.IV);
                cryptoAes.GenerateIV();
                return $"{ivText}{SalaryEncryptionDelimeter}{encryptedText}";
            }).ToArray();

            return encryptedSalaries;
        }
    }
}
