﻿using CommandLine;
using EmpInfo_Migration.DbContexts;
using NetCore.Tools.Migration.DbContexts.Payroll;
using NetCore.Tools.Migration.Helpers;
using NetCore.Tools.Migration.Models;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace NetCore.Tools.Migration
{
    public class MigrationService
    {
        private const double TrueCcCover = 34;
        private const double FalseCcCover = 23.5;

        private readonly ILogger _logger;
        private readonly EmpInfoDbContext _empInfoDbContext;
        private readonly PayrollDbContext _payrollDbContext;

        public MigrationService(ILogger logger, EmpInfoDbContext empInfoDbContext, PayrollDbContext payrollDbContext)
        {
            _logger = logger;
            _empInfoDbContext = empInfoDbContext;
            _payrollDbContext = payrollDbContext;
        }

        public void Run(string[] args)
        {
            RunMigration();
        }

        private void RunMigration()
        {
            ProcessSalaryMigration();
            MigrateCompulsory();
            Console.WriteLine("Migrate successfully !");
        }
        private void ProcessSalaryMigration()
        {
            var contracts = _empInfoDbContext.Contracts.Where(x => x.IsSalaryChange && !string.IsNullOrEmpty(x.GrossSalary)
                                                               && !string.IsNullOrEmpty(x.NetSalary) && !string.IsNullOrEmpty(x.BaseSalary)
                                                               && !x.IsDeleted)
                                                      .Select(x => new ContractModel
                                                      {
                                                          EmpCode = x.Employee.User.EmpCode,
                                                          ChangeContractDate = x.ChangeContractDate,
                                                          NetSalary = DecryptionHelper.Decode(x.NetSalary),
                                                          GrossSalary = DecryptionHelper.Decode(x.GrossSalary),
                                                          BaseSalary = DecryptionHelper.Decode(x.BaseSalary),
                                                          SocialEnsurancePercentage = x.SocialEnsurancePercentage
                                                      }).ToArray();

            var employees = _payrollDbContext.Employees.ToArray();
            var employeeBatches = GetBatches(employees, 20);

            foreach (var employeeBatche in employeeBatches)
            {
                MigrateSalary(employeeBatche, contracts);
            }
        }
        private void MigrateSalary(List<EmployeeEntity> employees, ContractModel[] contracts)
        {
            using (var transaction = _payrollDbContext.Database.BeginTransaction())
            {
                var salaryEntities = new List<SalaryEntity>();

                try
                {
                    foreach (var employee in employees)
                    {
                        var migratingContracts = contracts.Where(x => x.EmpCode == employee.EmpCode).ToArray();

                        var employeeSalary = new List<SalaryEntity>();

                        foreach (var migratingContract in migratingContracts)
                        {
                            var salaryEntity = new SalaryEntity
                            {
                                EmployeeId = employee.Id,
                                ApplyDate = migratingContract.ChangeContractDate,
                                CreatedTime = DateTimeOffset.UtcNow
                            };

                            var rawSalaries = new decimal[] { migratingContract.NetSalary, migratingContract.GrossSalary, migratingContract.BaseSalary };
                            var enpryptedSalaries = EncryptionHelper.EncryptEmployeeSalaries(rawSalaries, AppSettingsModel.PayrollSalaryEncryptKey);

                            salaryEntity.EncryptedNetSalary = enpryptedSalaries[0];
                            salaryEntity.EncryptedGrossSalary = enpryptedSalaries[1];
                            salaryEntity.EncryptedBaseSalary = enpryptedSalaries[2];

                            if (migratingContract.SocialEnsurancePercentage == null || migratingContract.SocialEnsurancePercentage == TrueCcCover)
                            {
                                salaryEntity.CcFullCover = true;
                            }
                            else if (migratingContract.SocialEnsurancePercentage == FalseCcCover)
                            {
                                salaryEntity.CcFullCover = false;
                            }

                            salaryEntities.Add(salaryEntity);
                            employeeSalary.Add(salaryEntity);
                        }

                        var salaryList = employeeSalary.OrderBy(o => o.ApplyDate).ToArray();
                        var lastIdx = salaryList.Length - 1;

                        for (var i = 0; i < salaryList.Length; i++)
                        {
                            var salary = salaryList[i];
                            var nextSalary = i == lastIdx ? null : salaryList[i + 1];

                            if (nextSalary != null)
                            {
                                salary.LastApplyDate = nextSalary.ApplyDate.Subtract(TimeSpan.FromDays(1));
                            }
                        }
                    }

                    _payrollDbContext.Salaries.AddRange(salaryEntities);
                    _payrollDbContext.SaveChanges();
                    transaction.Commit();

                    _logger.Information($"Salaries:  {JsonSerializer.Serialize(salaryEntities)}");
                    _logger.Information($"Completed: {salaryEntities.Count} employees");
                    Console.WriteLine($"Completed: {salaryEntities.Count} employees");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.Error($"Salaries: {ex.Message}");
                    _logger.Error($"Salaries: {JsonSerializer.Serialize(salaryEntities)}");
                }
            }
        }

        private void MigrateCompulsory()
        {
            var defaultPercentage = 32;
            var socialInsurances = _empInfoDbContext.SocialEnsurances.Where(x => !x.IsDeleted && x.Percentage == defaultPercentage).ToArray();

            using (var transaction = _payrollDbContext.Database.BeginTransaction())
            {
                var CompulsoryContributionEntities = new List<CompulsoryContributionEntity>();

                try
                {
                    foreach (var compulsoryContribution in socialInsurances)
                    {
                        var socialInsurance = new CompulsoryContributionEntity
                        {
                            CcType = CompulsoryContributionType.SocialInsurance,
                            EmployerRate = 17,
                            EmployerSalaryCapInVnd = 29800000,
                            EmployeeRate = 8,
                            EmployeeSalaryCapInVnd = 29800000,
                            ApplyDate = compulsoryContribution.ApplyStartDate,
                            CreatedTime = DateTimeOffset.UtcNow
                        };
                        CompulsoryContributionEntities.Add(socialInsurance);

                        var healthInsurance = new CompulsoryContributionEntity
                        {
                            CcType = CompulsoryContributionType.HealthInsurance,
                            EmployerRate = 3,
                            EmployerSalaryCapInVnd = 29800000,
                            EmployeeRate = 1.5,
                            EmployeeSalaryCapInVnd = 29800000,
                            ApplyDate = compulsoryContribution.ApplyStartDate,
                            CreatedTime = DateTimeOffset.UtcNow
                        };
                        CompulsoryContributionEntities.Add(healthInsurance);

                        var unemployementInsurance = new CompulsoryContributionEntity
                        {
                            CcType = CompulsoryContributionType.UnemploymentInsurance,
                            EmployerRate = 1,
                            EmployerSalaryCapInVnd = 88400000,
                            EmployeeRate = 1,
                            EmployeeSalaryCapInVnd = 88400000,
                            ApplyDate = compulsoryContribution.ApplyStartDate,
                            CreatedTime = DateTimeOffset.UtcNow
                        };
                        CompulsoryContributionEntities.Add(unemployementInsurance);

                        var tradeUnionFee = new CompulsoryContributionEntity
                        {
                            CcType = CompulsoryContributionType.TradeUnionFee,
                            EmployerRate = 2,
                            EmployerSalaryCapInVnd = 29800000,
                            EmployeeRate = 1,
                            EmployeeSalaryCapInVnd = 2500000,
                            ApplyDate = compulsoryContribution.ApplyStartDate,
                            CreatedTime = DateTimeOffset.UtcNow
                        };
                        CompulsoryContributionEntities.Add(tradeUnionFee);
                    }

                    _payrollDbContext.CompulsoryContributions.AddRange(CompulsoryContributionEntities);
                    _payrollDbContext.SaveChanges();
                    transaction.Commit();

                    _logger.Information($"Compulsory Contributions: {JsonSerializer.Serialize(CompulsoryContributionEntities)}");
                    _logger.Information($"Completed: {CompulsoryContributionEntities.Count} Compulsory Contributions");
                    Console.WriteLine($"Completed: {CompulsoryContributionEntities.Count} Compulsory Contributions");
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _logger.Error($"Compulsory Contributions:  {ex.Message}");
                    _logger.Error($"Compulsory Contributions:  {JsonSerializer.Serialize(CompulsoryContributionEntities)}");
                }
            }
        }
        private List<List<T>> GetBatches<T>(IEnumerable<T> originList, int subListLength)
        {
            var batches = new List<List<T>>();
            var length = originList.ToList().Count;

            if (subListLength > 0 && length > 0)
            {
                var currentPosition = 0;

                while (length != currentPosition)
                {
                    var subList = originList.Skip(currentPosition).Take(subListLength).ToList();
                    batches.Add(subList);
                    currentPosition += subList.Count;
                }

                return batches;
            }

            return batches;
        }
    }
}
